﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.Models
{
    public class SettingsDesignerItem : DesignerItemBase
    {
        public SettingsDesignerItem()
        {

        }
        public SettingsDesignerItem(SettingsDesignerItemViewModel item) : base(item)
        {
            this.Setting = item.Setting;
        }

        public string Setting { get; set; }
    }
}
