﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class GridControl : Grid
    {
        public IDiagramViewModel DiagramViewModel
        {
            get
            {
                return (this.DataContext as SelectableDesignerItemViewModelBase)?.Root;
            }
        }

        //protected override void OnPreviewKeyDown(KeyEventArgs e)
        //{
        //    base.OnPreviewKeyDown(e);

        //    e.Handled = DiagramViewModel?.ExecuteShortcut(e) ?? false;
        //}
    }
}
