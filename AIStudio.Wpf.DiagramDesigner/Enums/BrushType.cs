﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum BrushType
    {
        None,
        SolidColorBrush,
        LinearGradientBrush,
        RadialGradientBrush,
        ImageBrush,
        VisualBrush,
        DrawingBrush
    }
}
