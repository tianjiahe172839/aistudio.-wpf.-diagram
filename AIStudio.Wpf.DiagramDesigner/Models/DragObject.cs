﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner
{
    // Wraps info of the dragged object into a class
    public class DragObject
    {
        public Size? DesiredSize
        {
            get; set;
        }
        public Size? DesiredMinSize
        {
            get; set;
        }
        public Type ContentType
        {
            get; set;
        }
        public string Icon
        {
            get; set;
        }
        public string Text
        {
            get; set;
        }
        public IColorViewModel ColorViewModel
        {
            get; set;
        }
        public object DesignerItem
        {
            get; set;
        }
    }
}
