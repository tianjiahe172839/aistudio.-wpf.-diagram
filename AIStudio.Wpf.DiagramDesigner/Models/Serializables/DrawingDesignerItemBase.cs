﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DrawingDesignerItemBase : DesignerItemBase
    {
        public DrawingDesignerItemBase()
        {

        }
        public DrawingDesignerItemBase(DrawingDesignerItemViewModelBase item) : base(item)
        {
            this.Erasable = item.Erasable;
            this.Geometry = item.Geometry.ToString();
            if (item.Geometry.Transform != null)
            {
                this.Matrix = item.Geometry.Transform.Value.ToString();
            }
            this.Points = item.Points;
            this.DrawMode = item.DrawMode;
        }

        [XmlAttribute]
        public bool Erasable
        {
            get; set;
        }

        [XmlAttribute]
        public string Geometry
        {
            get; set;
        }

        [XmlAttribute]
        public string Matrix
        {
            get; set;
        }
        //[JsonIgnore]
        //[XmlElement("Matrix")]
        //public string XmlMatrix
        //{
        //    get
        //    {
        //        return SerializeHelper.SerializeMatrix(Matrix);
        //    }
        //    set
        //    {
        //        Matrix = SerializeHelper.DeserializeMatrix(value);
        //    }
        //}

        [XmlIgnore]
        public List<Point> Points
        {
            get; set;
        }

        [JsonIgnore]
        [XmlElement("Points")]
        public string XmlPoints
        {
            get
            {
                return SerializeHelper.SerializePointList(Points);
            }
            set
            {
                Points = SerializeHelper.DeserializePointList(value);
            }
        }

        [XmlAttribute]
        public DrawMode DrawMode
        {
            get; set;
        }
    }
}
