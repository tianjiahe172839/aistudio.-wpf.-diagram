﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IQuickThemeViewModel
    {
        QuickTheme[] QuickThemes { get; }
        QuickTheme QuickTheme { get; set; }
        event PropertyChangedEventHandler PropertyChanged;

    }
}
