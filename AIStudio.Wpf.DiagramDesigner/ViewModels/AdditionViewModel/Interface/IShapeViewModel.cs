﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IShapeViewModel
    {
        ISharpPath SourceMarker
        {
            get; set;
        }

        ISharpPath SinkMarker
        {
            get; set;
        }

        event PropertyChangedEventHandler PropertyChanged;
    }


}
