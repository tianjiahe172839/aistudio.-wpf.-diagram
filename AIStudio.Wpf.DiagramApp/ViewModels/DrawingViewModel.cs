﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.DiagramApp.ViewModels
{
    public class DrawingViewModel : PageViewModel
    {
        public DrawingViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {

        }
        public DrawingViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {
            foreach (var vm in DiagramViewModels)
            {
                vm.Init(false);
            }
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.DiagramOption.LayoutOption.GridCellSize = new Size(100, 100);
            DiagramViewModel.DiagramOption.LayoutOption.ShowGrid = true;
            DiagramViewModel.DiagramOption.LayoutOption.AllowDrop = true;
            _service.DrawModeViewModel.DrawingDrawModeSelected = true;
            _service.DrawModeViewModel.DrawingDrawMode = DrawMode.Select;
        }


        protected override void Init(bool initNew)
        {
            base.Init(initNew);

            DesignerItemViewModelBase line = new LineDrawingDesignerItemViewModel(DiagramViewModel, new List<Point> { new Point(38, 38), new Point(118, 118) }, null, true);
            DiagramViewModel.Add(line);

            DesignerItemViewModelBase rectangle = new RectangleDrawingDesignerItemViewModel(DiagramViewModel, new List<Point> { new Point(138, 38), new Point(218, 118) }, null, true);
            DiagramViewModel.Add(rectangle);

            DesignerItemViewModelBase ellipse = new EllipseDrawingDesignerItemViewModel(DiagramViewModel, new List<Point> { new Point(238, 38), new Point(318, 118) }, null, true);
            DiagramViewModel.Add(ellipse);

            DesignerItemViewModelBase text = new TextDrawingDesignerItemViewModel(DiagramViewModel, new Point(338, 38), "以下图形使用路线", null, true);
            DiagramViewModel.Add(text);

            DesignerItemViewModelBase triangle = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableTriangle, new List<Point> { new Point(438, 38), new Point(518, 118) }, null, true);
            DiagramViewModel.Add(triangle);

            DesignerItemViewModelBase rhombus = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableRhombus, new List<Point> { new Point(538, 38), new Point(618, 118) }, null, true);
            DiagramViewModel.Add(rhombus);

            DesignerItemViewModelBase hexagon = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableHexagon, new List<Point> { new Point(638, 38), new Point(718, 118) }, null, true);
            DiagramViewModel.Add(hexagon);

            DesignerItemViewModelBase pentagram = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasablePentagram, new List<Point> { new Point(738, 38), new Point(818, 118) }, null, true);
            DiagramViewModel.Add(pentagram);

            DesignerItemViewModelBase starFour = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableStarFour, new List<Point> { new Point(38, 138), new Point(118, 218) }, null, true);
            DiagramViewModel.Add(starFour);

            DesignerItemViewModelBase starThree = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableStarThree, new List<Point> { new Point(138, 138), new Point(218, 218) }, null, true);
            DiagramViewModel.Add(starThree);

            DesignerItemViewModelBase chat = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableChat, new List<Point> { new Point(238, 138), new Point(318, 218) }, null, true);
            DiagramViewModel.Add(chat);

            DesignerItemViewModelBase comment = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableComment, new List<Point> { new Point(338, 138), new Point(418, 218) }, null, true);
            DiagramViewModel.Add(comment);

            DesignerItemViewModelBase cloud = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableCloud, new List<Point> { new Point(438, 138), new Point(518, 218) }, null, true);
            DiagramViewModel.Add(cloud);

            DesignerItemViewModelBase arrowRight = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableArrowRight, new List<Point> { new Point(538, 138), new Point(618, 218) }, null, true);
            DiagramViewModel.Add(arrowRight);

            DesignerItemViewModelBase arrowLeft = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableArrowLeft, new List<Point> { new Point(638, 138), new Point(718, 218) }, null, true);
            DiagramViewModel.Add(arrowLeft);

            DesignerItemViewModelBase check = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableCheck, new List<Point> { new Point(738, 138), new Point(818, 218) }, null, true);
            DiagramViewModel.Add(check);

            DesignerItemViewModelBase close = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableClose, new List<Point> { new Point(38, 238), new Point(118, 318) }, null, true);
            DiagramViewModel.Add(close);

            DesignerItemViewModelBase heart = new SharpDrawingDesignerItemViewModel(DiagramViewModel, DrawMode.ErasableHeart, new List<Point> { new Point(138, 238), new Point(218, 318) }, null, true);
            DiagramViewModel.Add(heart);

            DesignerItemViewModelBase polyline = new PolylineDrawingDesignerItemViewModel(DiagramViewModel, new List<Point> { new Point(238, 238), new Point(298, 258), new Point(258, 298), new Point(318, 318) }, null, true);
            DiagramViewModel.Add(polyline);

            DesignerItemViewModelBase polygon = new PolygonDrawingDesignerItemViewModel(DiagramViewModel, new List<Point> { new Point(338, 238), new Point(398, 258), new Point(358, 298), new Point(418, 318) }, null, true);
            DiagramViewModel.Add(polygon);

            DesignerItemViewModelBase text2 = new TextDrawingDesignerItemViewModel(DiagramViewModel, new Point(438, 238), "以下图形使用画线", null, true);
            DiagramViewModel.Add(text2);

            //画正方形
            var drawingPoint1 = DrawingHelper.GetPoints(new Point(38, 518), 0, Enumerable.Repeat(new System.Tuple<double, double>(160, -90), 4).ToList());
            DesignerItemViewModelBase directLine1 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint1, null, true);
            DiagramViewModel.Add(directLine1);

            //画三角形
            var drawingPoint2 = DrawingHelper.GetPoints(new Point(238, 518), 0, Enumerable.Repeat(new System.Tuple<double, double>(160, -120), 3).ToList());
            DesignerItemViewModelBase directLine2 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint2, null, true);
            DiagramViewModel.Add(directLine2);

            //画五边形
            var drawingPoint3 = DrawingHelper.GetPoints(new Point(478, 518), 0, Enumerable.Repeat(new System.Tuple<double, double>(100, -72), 5).ToList());
            DesignerItemViewModelBase directLine3 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint3, null, true);
            DiagramViewModel.Add(directLine3);

            //画六角形
            List<System.Tuple<double, double>> tuple4 = new List<System.Tuple<double, double>>();
            for (int i = 0; i < 5; i++)
            {
                tuple4.Add(new System.Tuple<double, double>(70, 144));
                tuple4.Add(new System.Tuple<double, double>(70, -72));
            }
            var drawingPoint4 = DrawingHelper.GetPoints(new Point(750, 408), 0, tuple4);
            DesignerItemViewModelBase directLine4 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint4, null, true);
            DiagramViewModel.Add(directLine4);

            //画八边形
            List<System.Tuple<double, double>> tuple5 = new List<System.Tuple<double, double>>();
            for (int i = 0; i < 8; i++)
            {
                tuple5.Add(new System.Tuple<double, double>(36, -90));
                tuple5.Add(new System.Tuple<double, double>(36, 45));
            }
            var drawingPoint5 = DrawingHelper.GetPoints(new Point(153, 689), 0, tuple5);
            DesignerItemViewModelBase directLine5 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint5, null, true);
            DiagramViewModel.Add(directLine5);

            //画6个正三角形
            List<System.Tuple<double, double>> tuple6 = new List<System.Tuple<double, double>>();
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    tuple6.Add(new System.Tuple<double, double>(80, -120));
                }
                tuple6.Add(new System.Tuple<double, double>(0, -60));
            }
            var drawingPoint6 = DrawingHelper.GetPoints(new Point(328, 628), 0, tuple6);
            DesignerItemViewModelBase directLine6 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint6, null, true);
            DiagramViewModel.Add(directLine6);

            //画6个正六边形
            List<System.Tuple<double, double>> tuple7 = new List<System.Tuple<double, double>>();
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    tuple7.Add(new System.Tuple<double, double>(42, -60));
                }
                tuple7.Add(new System.Tuple<double, double>(0, -60));
            }
            var drawingPoint7 = DrawingHelper.GetPoints(new Point(528, 628), 0, tuple7);
            DesignerItemViewModelBase directLine7 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint7, null, true);
            DiagramViewModel.Add(directLine7);

            //画8个八角星
            List<System.Tuple<double, double>> tuple8 = new List<System.Tuple<double, double>>();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tuple8.Add(new System.Tuple<double, double>(20, -90));
                    tuple8.Add(new System.Tuple<double, double>(20, 45));
                }
                tuple8.Add(new System.Tuple<double, double>(0, -45));
            }
            var drawingPoint8 = DrawingHelper.GetPoints(new Point(728, 628), 0, tuple8);
            DesignerItemViewModelBase directLine8 = new DirectLineDrawingDesignerItemViewModel(DiagramViewModel, drawingPoint8, null, true);
            DiagramViewModel.Add(directLine8);

            DesignerItemViewModelBase text3 = new TextDrawingDesignerItemViewModel(DiagramViewModel, new Point(38, 738), "示例完毕，欢迎大家来到AIStudio画笔画板", null, true);
            DiagramViewModel.Add(text3);
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
