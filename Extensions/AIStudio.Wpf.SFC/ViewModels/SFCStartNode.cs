﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class SFCStartNode : SFCNode
    {
        public SFCStartNode() : this(null)
        {
          
        }

        public SFCStartNode(IDiagramViewModel root) : base(root, SFCNodeKinds.Start)
        {
            ExecuteAddTopInput(null);
            ExecuteAddBottomOutput(null);
        }

        public SFCStartNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public SFCStartNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }
    }
}
