﻿using System.Globalization;
using System.Windows.Data;
using System;
using System.Windows.Input;
using System.Windows.Controls;

namespace  AIStudio.Wpf.Mind.Controls
{
    public class ColorPickerTabItem : TabItem
    {
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e.Source == this || !this.IsSelected)
                return;

            base.OnMouseLeftButtonDown(e);
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            //Selection on Mouse Up
            if (e.Source == this || !this.IsSelected)
            {
                base.OnMouseLeftButtonDown(e);
            }

            base.OnMouseLeftButtonUp(e);
        }
    }

    public class ColorModeToTabItemSelectedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorMode = (ColorMode)value;
            return (colorMode == ColorMode.ColorPalette) ? 0 : 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var index = (int)value;
            return (index == 0) ? ColorMode.ColorPalette : ColorMode.ColorCanvas;
        }
    }
}
