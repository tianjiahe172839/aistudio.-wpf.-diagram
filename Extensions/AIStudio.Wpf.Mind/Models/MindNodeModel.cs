﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Windows;
using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Serializable;
using AIStudio.Wpf.DiagramDesigner.Serializable.ViewModels;
using AIStudio.Wpf.Mind;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Models
{

    public class MindNodeModel : DiagramNode
    {
        public MindType MindType
        {
            get; set;
        }

        public MindTheme MindTheme
        {
            get; set;
        }

        public Size Spacing
        {
            get; set;
        }


        public Point Offset
        {
            get; set;
        }

        public bool IsExpanded
        {
            get; set;
        }

        public LinkInfoModel LinkInfoModel
        {
            get; set;
        }

        public ImageInfoModel ImageInfoModel
        {
            get; set;
        }

        public string Remark
        {
            get; set;
        }

        public double? Priority
        {
            get; set;
        }

        public double? Rate
        {
            get; set;
        }

        public List<string> Tags
        {
            get; set;
        }

        public override DiagramItemViewModel ToNodel(IDiagramViewModel diagramViewModel)
        {
            MindNode mindNode = new MindNode(diagramViewModel);
            mindNode.MindType = MindType;
            mindNode.MindTheme = MindTheme;
            mindNode.Spacing = Spacing;
            mindNode.Offset = Offset;
            mindNode.IsExpanded = IsExpanded;
            if (string.IsNullOrEmpty(LinkInfoModel?.Url) && string.IsNullOrEmpty(LinkInfoModel?.Text))
            {
                mindNode.LinkInfo = null;
            }
            else
            {
                mindNode.LinkInfo = new LinkInfo(LinkInfoModel?.Url, LinkInfoModel?.Text);
            }
            if (string.IsNullOrEmpty(ImageInfoModel?.Url) && string.IsNullOrEmpty(ImageInfoModel?.Text))
            {
                mindNode.ImageInfo = null;
            }
            else
            {
                mindNode.ImageInfo = new ImageInfo(ImageInfoModel?.Url, ImageInfoModel?.Text);
            }
            mindNode.Remark = Remark;
            mindNode.Priority = Priority;
            mindNode.Rate = Rate;
            if (Tags != null)
            {
                mindNode.Tags = new System.Collections.ObjectModel.ObservableCollection<string>(Tags);
            }

            mindNode.InitLayout(false);
            return mindNode;
        }
    }

    public class LinkInfoModel
    {
        public LinkInfoModel()
        {

        }

        public LinkInfoModel(LinkInfo linkinfo)
        {
            Url = linkinfo?.Url;
            Text = linkinfo?.Text;
        }

        public string Url
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }
    }

    public class ImageInfoModel
    {
        public ImageInfoModel()
        {

        }

        public ImageInfoModel(ImageInfo imageInfo)
        {
            Url = imageInfo?.Url;
            Text = imageInfo?.Text;
        }

        public string Url
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }
    }
}
